Ref: https://ng-bootstrap.github.io/#/getting-started
uninstalling previos installations of bootstrap 3
cmd>   npm remove ng2-bootstrap




Ref: https://github.com/angular/angular-cli

## Install Bootstrap 
Install bootstrap under node_modules
```sh
Install latest bootstrap 4
npm install --save bootstrap@next 

Install latest angular2 boostrap components
npm install --save @ng-bootstrap/ng-bootstrap

```

## Include to angular-cli.json file
```json
"styles": [
    "../node_modules/bootstrap/dist/css/bootstrap.css",
    "assets/css/font-awesome.min.css",
    "assets/css/styles.css"
],
```

## Re-start the server 
```sh
~/ng2/> ng serve
```


# setting up
File : app.module.ts
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [AppComponent, ...],
  imports: [NgbModule.forRoot(), ...],
  bootstrap: [AppComponent]
})
export class AppModule {
}