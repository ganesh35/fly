Globals:
File: fly/src/app/common/globals.ts
```javascript
export var languageList = [
    {lang:'en', 'flag':'en.gif', title: "English" },
    {lang:'ru', 'flag':'de.gif', title: "Russian" } 
];
export var languageDefault = 'en';
export var languageFallback = 'en';
export var apiBaseUrl = "https://demo.calculustechnologies.com/FZPackageAPI/WS/PackageAPI.asmx"
```

-----------------------------------------------------
## Multi language

### Install
npm install ng2-translate --save



### Language Files
File: fly/src/i18n/en.json
{
    "HELLO": "hello {{value}}"
}

File: ng2/src/i18n/ru.json
{
    "HELLO": "Алло! {{value}}"
}

### app.module.ts
import {BrowserModule} from "@angular/platform-browser";
import {NgModule} from '@angular/core';
import {HttpModule} from '@angular/http';
import {TranslateModule} from 'ng2-translate';

@NgModule({
    imports: [
        BrowserModule,
        HttpModule,
        TranslateModule.forRoot()
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}

### app.component.ts
...
	import {TranslateService} from 'ng2-translate';
  import * as myGlobals from './common/globals'; 
...
  	public langList = [];
  	public currentLang = '';
  	constructor(private translate: TranslateService) {
        translate.setDefaultLang(myGlobals.languageFallback);
        translate.use(myGlobals.languageDefault);
    }
	ngOnInit(){
      this.langList = myGlobals.languageList;
    }
 	onLangChange(val){
    	this.translate.use(val);
        this.currentLang=val;
    } 
...


### app.component.html
{{'HELLO' | translate}}

<div [innerHTML]="'HELLO' | translate"></div>



For Language switcher:
### app.component.html

                <small *ngFor="let item of langList">
                    <span *ngIf="currentLang==item.lang" >
                        <i class="fa fa-check" aria-hidden="true"></i> {{item.title}}
                    </span>
                    <span *ngIf="currentLang!=item.lang" >
                        <a (click)="onLangChange(item.lang)" style="cursor: pointer; cursor: hand; " class="">
                        {{item.title}}
                        </a>
                    </span>
                    &nbsp; &nbsp;
                </small>