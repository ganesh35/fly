

1. Create project with 
ng new fly

2. Creating components

HomeComponent: 
File: fly/src/app/components/home/home.component.ts
------------------------------------------------------------------------
import { Component, OnInit } from '@angular/core';
@Component({
  templateUrl: './home.component.html',
})
export class HomeComponent implements OnInit {
	constructor() { }
	ngOnInit() {
	}
}
------------------------------------------------------------------------
File: fly/src/app/components/home/home.component.html
------------------------------------------------------------------------
<p>Home works!</p>
------------------------------------------------------------------------



ListingComponent: 
File: fly/src/app/components/listing/listing.component.ts
------------------------------------------------------------------------
import { Component, OnInit } from '@angular/core';
@Component({
  templateUrl: './listing.component.html',
})
export class ListingComponent implements OnInit {
	constructor() { }
	ngOnInit() {
	}
}
------------------------------------------------------------------------
File: fly/src/app/components/listing/listing.component.html
------------------------------------------------------------------------
<p>Listing works!</p>
------------------------------------------------------------------------



MapComponent: 
File: fly/src/app/components/map/map.component.ts
------------------------------------------------------------------------
import { Component, OnInit } from '@angular/core';
@Component({
  templateUrl: './map.component.html',
})
export class MapComponent implements OnInit {
	constructor() { }
	ngOnInit() {
	}
}
------------------------------------------------------------------------
File: fly/src/app/components/map/map.component.html
------------------------------------------------------------------------
<p>Map works!</p>
------------------------------------------------------------------------

NopageComponent: 
File: fly/src/app/components/nopage/nopage.component.ts
------------------------------------------------------------------------
import { Component, OnInit } from '@angular/core';
@Component({
  templateUrl: './nopage.component.html',
})
export class NopageComponent implements OnInit {
	constructor() { }
	ngOnInit() {
	}
}
------------------------------------------------------------------------
File: fly/src/app/components/nopage/nopage.component.html
------------------------------------------------------------------------
<p>Nopage works!</p>
------------------------------------------------------------------------


3. Update app.module.ts
File: fly/src/app/app.module.ts
------------------------------------------------------------------------
...
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { ListingComponent } from './components/listing/listing.component';
import { MapComponent } from './components/map/map.component';
import { NopageComponent } from './components/nopage/nopage.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ListingComponent,
    MapComponent,
    NopageComponent
  ],
  ...
------------------------------------------------------------------------  