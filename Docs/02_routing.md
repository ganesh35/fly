# Implementation of Routing 
## Step #1: Create necessary components first
## Step #2: Make sure the components are included in the app.module.ts
## Step #3: Create a new file app.routing.ts
File : fly/src/app/app.routing.ts
```javascript
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './components/home/home.component';
import { ListingComponent } from './components/listing/listing.component';
import { MapComponent } from './components/map/map.component';
import { NopageComponent } from './components/nopage/nopage.component';

const appRoutes: Routes = [
    { path: '',    component: HomeComponent},
  	{ path: 'listing',    component: ListingComponent},
    { path: 'map',    component: MapComponent},
    { path: 'error/:page', component: NopageComponent },
  	{ path: '**', component: NopageComponent } 	
];
export const routing = RouterModule.forRoot(appRoutes);
```

## Step #4: include app.routing to app.module

```javascript
...
import {routing} from './app.routing';
...
imports:      [ BrowserModule, routing ],
...
```

##  Step #5: Add base href to index.html

```html
<head>
  <base href="/">
```

##  Step #6: create router-outlet   
File : fly/src/app/app.component.html  
Include router-outline where the page content goes
```html
<body>
...
<router-outlet></router-outlet>
...
</body>
...
```

## Step #7: Create menu items  
File: fly/src/app/app.component.html
```html
...
<a class="nav-link" routerLink="/" routerLinkActive="active"><i class="glyphicon glyphicon-home"></i> Home </a>
<a class="nav-link" routerLink="/listing" routerLinkActive="active"><i class="glyphicon glyphicon-home"></i> Listing </a>
<a class="nav-link" routerLink="/map" routerLinkActive="active"><i class="glyphicon glyphicon-home"></i> Map </a>
...
```



