import { FlyPage } from './app.po';

describe('fly App', function() {
  let page: FlyPage;

  beforeEach(() => {
    page = new FlyPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
