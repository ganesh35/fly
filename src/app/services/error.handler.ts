import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
@Injectable()
export class ErrorHandler {
    constructor(private _router: Router) { }
    HttpError(httpError: any){
        if (httpError.status == 0) {
            this._router.navigate(['/error/server-down']);
        } 
        if (httpError.status == 401) {
            this._router.navigate(['/']);
        }
        return httpError;
    }
}