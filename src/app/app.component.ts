import { Component } from '@angular/core';
import {TranslateService} from 'ng2-translate';
import * as myGlobals from './common/globals'; 

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent {
    public langList = [];
    public currentLang = '';
    constructor(private translate: TranslateService) {
        translate.setDefaultLang(myGlobals.languageFallback);
        translate.use(myGlobals.languageDefault);
        this.currentLang=myGlobals.languageDefault
    }
  ngOnInit(){
      this.langList = myGlobals.languageList;
    }
  onLangChange(){
      this.translate.use(this.currentLang);
        //this.currentLang=val;
    } 
}
