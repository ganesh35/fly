import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './components/home/home.component';
import { ListingComponent } from './components/listing/listing.component';
import { MapComponent } from './components/map/map.component';
import { NopageComponent } from './components/nopage/nopage.component';

const appRoutes: Routes = [
    { path: '',    component: HomeComponent},
  	{ path: 'listing',    component: ListingComponent},
    { path: 'map',    component: MapComponent},
    { path: 'error/:page', component: NopageComponent },
  	{ path: '**', component: NopageComponent } 	
];
export const routing = RouterModule.forRoot(appRoutes);