import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { FlightSearchComponent } from './components/flightsearch/flightsearch.component';
import { ListingComponent } from './components/listing/listing.component';
import { MapComponent } from './components/map/map.component';
import { NopageComponent } from './components/nopage/nopage.component';
import {routing} from './app.routing';

import {TranslateModule} from 'ng2-translate';

import {ErrorHandler } from './services/error.handler'
import {FatService } from './services/fat.service';


import { ReactiveFormsModule } from '@angular/forms';
import { TypeaheadModule } from 'ng2-bootstrap';
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ListingComponent,
    MapComponent,
    NopageComponent,
    FlightSearchComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing,
    TranslateModule.forRoot(),
    ReactiveFormsModule,
    TypeaheadModule.forRoot(),
  ],
  providers: [ErrorHandler,FatService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
