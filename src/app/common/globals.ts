export var languageList = [
	{lang:'en', 'flag':'en.gif', title: "English (AED)" },
    {lang:'ru', 'flag':'de.gif', title: "Russian (RUB)" },
    {lang:'ar', 'flag':'ar.gif', title: "Arabic " }
];
export var languageDefault = 'en';
export var languageFallback = 'en';
export var apiBaseUrl = "https://demo.calculustechnologies.com/FZPackageAPI/WS/PackageAPI.asmx/"
//export var apiBaseUrl = "http://api.fixmygadgets.in:8080/api/1.0.0/"


export var cabinClass = ["Economy", "Business"];
