export class SearchFlight {
    public DepartureAirportCode: string;
    public ArrivalAirportCode: string;
	public TravelClass: string;
    public sDepartureDate: any;
    public sReturnDate: any;
    public IsReturnJourney: boolean;
    public AdultCount: number;
    public ChildCount: number;
    public InfantCount: number;
    public IsDirectFlightOnly: boolean;
    constructor(){
        this.AdultCount = 1;
        this.DepartureAirportCode = "";
        this.ArrivalAirportCode = "";

        this.sDepartureDate = new Date();
        this.sDepartureDate.setDate(this.sDepartureDate.getDate() + 1);
        this.sReturnDate = new Date();
        this.sReturnDate.setDate(this.sReturnDate.getDate() + 6);
        this.TravelClass = "Economy";


    }
}
