import { Component, OnInit } from '@angular/core';
import { Title }     from '@angular/platform-browser';
import { FatService }       from '../../services/fat.service';
import { ErrorHandler }       from '../../services/error.handler';

import { SearchFlight }              from './flightsearch.interface';

import { FormGroup, FormBuilder, Validators } from '@angular/forms';


@Component({
  selector: 'app-flightsearch',
  templateUrl: './flightsearch.component.html'
})
export class FlightSearchComponent implements OnInit {

    constructor(
        private _titleService: Title, 
        private _fatService: FatService,
        private _errHandler: ErrorHandler,
        private fb: FormBuilder
    ) { }


    public loading = false;
    public errorMessage:any;
    public successMessage ='';
    public cities: any;


    public DepartureAirportCode: string;
    public ArrivalAirportCode: string;

    public item = new SearchFlight();
    public items: any;
    ngOnInit() {
        this._titleService.setTitle( "Search Flights" );
        this.getCities("Duba");
        this.buildForm();


        //this.getSearchResults();




    }
    rePopulateCities(field){
        if (this.item[field].length >=2){
            this.getCities(this.item[field])
        }
      
    }
    getCities(key: string) {
        this.successMessage = "";
        this.errorMessage = null;
        this.loading = true;
        let apiUrl=  'LookupPackageDestinationCountryCity'
        //let apiUrl=  'services'

        let requestRawData = {"lookupKey":key}
        this._fatService.post( apiUrl, requestRawData)
        .subscribe(
            res => { 
              this.cities = res.d.FetchedObjects; 
              this.loading = false;
            },
            error =>  {
              this.errorMessage = this._errHandler.HttpError(<any>error)
              this.loading = false;
            }
        );
    }   
    /*
  getSearchResults() {
        this.successMessage = "";
        this.errorMessage = null;
        this.loading = true;
        let apiUrl=  'SearchPackage'
        //let apiUrl=  'services'

        let requestRawData = {
  "GUID":"2657341a-f2b9-4783-89bb-e63642f72808",
  "Location":{
    "Key":"14", 
    "Value":"Dubai City, United Arab Emirates"
  },
  "sStartDtTm":"2017/02/20",
  "Destinations":{
    "CriteriaIndex":99999999999,
    "Location":{
      "Key":"22641", 
      "Value":"MALINSKA-DUBASNICA, Croatia"
    },
    "NoOfNights": 5,
    "sStartDtTm": ""
  },
  "TravelClass":"Economy",
  "MinStarRating":3,
  "Rooms": {
    "RoomIndex": 99999999,
    "AdultCount":1,
    "ChildCount":2,
    "ChildAges": [9, 7],
    "InfantCount": 2

  }
}
        this._fatService.post( apiUrl, requestRawData)
        .subscribe(
            res => { 
              this.cities = res.d.FetchedObjects; 
              this.loading = false;
            },
            error =>  {
              this.errorMessage = this._errHandler.HttpError(<any>error)
              this.loading = false;
            }
        );
    }   
*/

    
    ngForm: FormGroup;
    buildForm(): void {
        this.ngForm = this.fb.group({
          'DepartureAirportCode': [this.item.DepartureAirportCode, [
              Validators.required,
              Validators.minLength(3)
            ]
          ],
          'ArrivalAirportCode': [this.item.ArrivalAirportCode, [
              Validators.required,
              Validators.minLength(3)
            ]
          ],
        'sDepartureDate': [this.item.sDepartureDate, [
              Validators.required
            ]
          ],


        });

        this.ngForm.valueChanges
          .subscribe(data => this.onValueChanged(data));

        this.onValueChanged(); // (re)set validation messages now
    }    

    onValueChanged(data?: any) {
        if (!this.ngForm) { return; }
        const form = this.ngForm;

        for (const field in this.formErrors) {
          // clear previous error message (if any)
          this.formErrors[field] = '';
          const control = form.get(field);

          if (control && control.dirty && !control.valid) {
            const messages = this.validationMessages[field];
            for (const key in control.errors) {
              this.formErrors[field] += messages[key] + ' ';
            }
          }
        }
    }
  formErrors = {
        'DepartureAirportCode': '',
        'ArrivalAirportCode': '',
        'sDepartureDate': ''
    };

    validationMessages = {
        'DepartureAirportCode': {
          'required':      'Departure is required.',
          'minlength':     'Departure must be at least 3 characters long'
        },
        'ArrivalAirportCode': {
          'required':      'Arrival Airport is required.',
          'minlength':     'Arrival Airport must be at least 3 characters long'
        },
        'sDepartureDate': {
          'required':      'Departure date is required.'
        }    

    };       







  doSubmit(){
    console.log(this.item)
  }
}
