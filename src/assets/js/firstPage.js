'use strict';

require(["config"], function() {
    require(['jquery', 'owl', 'daterangepicker'], // Dependencies declared here would ALONE be loaded for 'firstPage', hence the network load is very less. (page loads much faster)
        function($, owl, daterangepicker) {

            var config = { // Page-level feature props
                caraousel: {
                    home: {
                        loop: true,
                        dots: true,
                        items: 1,
                        lazyLoad: true,
                        arrows: true
                    },
                    offer: {
                        loop: true,
                        dots: true,
                        items: 1,
                        lazyLoad: true,
                        arrows: true
                    },
                    recommended_stays: {
                        margin: 10,
                        loop: true,
                        dots: true,
                        items: 1,
                        arrows: true,
                        autoWidth: true
                    },
                    upcoming_trips: {
                        loop: true,
                        dots: true,
                        items: 1,
                        lazyLoad: true,
                        arrows: true
                    }
                },
                dateRangePicker: {
                    props: {
                        "autoApply": true,
                        "alwaysShowCalendars": true,
                        "opens": "center",
                        "orientation": "auto",
                        // 'minDate': '0',
                        "locale": {
                            // "direction": "rtl"
                            "format": "DD/MMM/YYYY",
                            "separator": " / ",
                            "daysOfWeek": ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
                            "monthNames": ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
                            "firstDay": 1
                        },
                        "isInvalidDate": function(date) {
                            var invalidDatesList = ['02-01-2017', '01-02-2017', '03-03-2017', '05-04-2017'];
                            for (var i = 0; i < invalidDatesList.length; i++) {
                                if (date.format('DD-MM-YYYY') == invalidDatesList[i]) {
                                    return true;
                                }
                            }
                        },
                        "linkedCalendars": false,
                        "autoUpdateInput": true,
                        "showCustomRangeLabel": false
                    },
                    mobile: {
                        "singleDatePicker": true,
                        "opens": "center",
                        "drops": "up"
                    },
                     mobileRight: {
                        "singleDatePicker": true,
                        "opens": "left",
                        "drops": "up"
                    },
                    callback: function(start, end, label, type) {
                        var $current = $(this);
                        if (type === "mobile") {
                            $current.text(start.format('DD/MM/YYYY'))
                        } else {
                            $('#vacationDates').text(" ( " + start.format('DD/MM/YYYY') + " - " + end.format('DD/MM/YYYY') + " ) ");
                        }

                    }
                },
                roomPod: {
                    show: function(e) {
                        e.stopPropagation();
                        $(this).find('.room-pod').slideDown('fast');
                        $(document).on('click', config.roomPod.slideUp);
                    },
                    slideUp: function(e) {
                        e.stopPropagation();
                        $('.room-pod').slideUp('fast');
                        $(document).off('click', config.roomPod.slideUp); // unbinding event when in not use -- preventing memory leak
                    }
                }
            }

            // Carousel triggers
            $('.home-carousal').owlCarousel(config.caraousel.home);
            $('.offer-carousal').owlCarousel(config.caraousel.offer);
            $('#recommended_stays,#c-explore,#fz-excursions').owlCarousel(config.caraousel.recommended_stays);
            $('.trip-carousel').owlCarousel(config.caraousel.upcoming_trips);

            // Datepicker trigger

            $('.search-calendar').daterangepicker(config.dateRangePicker.props, config.dateRangePicker.callback);
            $('.checkin').daterangepicker(config.dateRangePicker.mobile);
             $('.checkout').daterangepicker(config.dateRangePicker.mobileRight);
            // Room-Pod trigger
            $('.search-guests').click(config.roomPod.show);
            $(".done,.close").click(config.roomPod.slideUp);

        });
});