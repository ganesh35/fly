'use strict';

require(["config"], function() {
    require(['jquery', 'globals'], // Dependencies declared here would ALONE be loaded for 'secondPage', hence the network load is very less. (page loads much faster)
        function($, g) {
            //map page js
            $('.view-switch p').html('<img src="./img/list-view.png">List View');
            $(window).resize(function(){
                 if($(window).width()<768){
                     $('.mv-details').removeClass('pull-right');
                }else{
                    $('.mv-details').addClass('pull-right');
                }
            });
        });
});