'use strict';

require(["config"], function() {
    require(['jquery', 'globals','noUiSlider'], // Dependencies declared here would ALONE be loaded for 'secondPage', hence the network load is very less. (page loads much faster)
        function($, g,noUiSlider) {
             
             noUiSlider.create($('#rb-line')[0], {
					start: [20, 70],
					connect: true,
					range: {
						'min': 0,
						'max': 100
					}
			});
            $(window).resize(function(){
                 if($(window).width()<768){
                     $('.flight-info').removeClass('flight-info-selected');
                     $('.fi-expanded').hide();
                }
            });
            $('.flight-info').click(function(){
                if($(window).width()<768){
                    return;
                }
                  if($(this).hasClass('flight-info-selected')){
                      $(this).removeClass('flight-info-selected')
                      $('.fi-expanded').hide();
                  }else{
                      $(this).parent().find('.flight-info').removeClass('flight-info-selected');
                      $(this).addClass('flight-info-selected');
                      $('.fi-expanded').show();
                  }                  
            });
            $('.lh-preference').click(function(){
                $('#filterBtn').trigger('click');
            });
            $('#filterBtn').click(function(){
             if($('body').hasClass('filterOpen')){
                    $('.lc-filter').hide();
                    $('#filterBtn').show();$('#sortBtn').show();
                    $('.filterBar').hide();
                    $('.lc-content').show();
                    $('.mobile-header-view').show();
                    $('.listing-content-mobile').show();
                    $('body').removeClass('filterOpen');
             }else{
                    $('.lc-filter').show();
                    $('#filterBtn').hide();$('#sortBtn').hide();
                    $('.filterBar').show();
                    $('.lc-content').hide();
                    $('.mobile-header-view').hide();
                    $('.listing-content-mobile').hide();
                    $('body').addClass('filterOpen');
             }
            });
            $('.toggle-switch span').click(function(){
                $(this).parent().find('span').removeClass('active');
                $(this).addClass('active');
            });
            $('.lcf-checks i').click(function(){
                var box = $(this).closest('.lcf-checks');
                var form =box.find('div').first();
                if(form.is(':visible')){
                    form.slideUp();
                    $(this).removeClass('fa-angle-up').addClass('fa-angle-down');
                }else{
                    form.slideDown();
                    $(this).removeClass('fa-angle-down').addClass('fa-angle-up');
                }
            });



        });
});