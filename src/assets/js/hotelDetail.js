'use strict';

require(["config"], function() {
    require(['jquery', 'globals', 'owl'], // Dependencies declared here would ALONE be loaded for 'secondPage', hence the network load is very less. (page loads much faster)
        function($, g, owl) {

            var config = {
                carousel: {
                    recommended_stays: {
                        margin: 10,
                        loop: true,
                        dots: true,
                        items: 1,
                        arrows: true,
                        autoWidth: true
                    },
                    goodtoknow: {
                        loop: true,
                        dots: true,
                        arrows: true,
                        responsiveClass: true,
                        responsive: {
                            0: {
                                items: 1,
                                nav: true
                            },
                            600: {
                                items: 2,
                                nav: false
                            },
                            1000: {
                                items: 3,
                                nav: true,
                                loop: false
                            }
                        }
                    }
                }
            }

            // Carousel triggers
            $('#recommended_stays').owlCarousel(config.carousel.recommended_stays);
            $('.goodtoknow-carousel').owlCarousel(config.carousel.goodtoknow);

            $('.lcf-checks i').click(function() {
                var box = $(this).closest('.lcf-checks');
                var form = box.find('form');
                if (form.is(':visible')) {
                    form.slideUp();
                    $(this).removeClass('fa-angle-down').addClass('fa-angle-up');
                    box.css('padding-bottom', '5px');
                } else {
                    form.slideDown();
                    $(this).removeClass('fa-angle-up').addClass('fa-angle-down');
                    box.css('padding-bottom', '40px');
                }
            });

            // hotel detail carousal
            var sync2 = $('#sync2'),
                sync1 = $('#sync1'),
                thumbs = 8,
                duration = 300,
                flag = false;
            sync1.on('click', '.owl-next', function() {
                sync2.trigger('next.owl.carousel')
            });
            sync1.on('click', '.owl-prev', function() {
                sync2.trigger('prev.owl.carousel')
            });

            // Start Carousel
            sync1.owlCarousel({
                    // center : true,
                    loop: true,
                    items: 1,
                    margin: 0,
                    nav: true,
                    dots: false,
                    autoWidth: false,
                    navText: ["&lsaquo;", "&rsaquo;"]
                })
                .on('dragged.owl.carousel', function(e) {
                    if (e.relatedTarget.state.direction == 'left') {
                        sync2.trigger('next.owl.carousel')
                    } else {
                        sync2.trigger('prev.owl.carousel')
                    }
                });


            sync2.owlCarousel({
                    // center: true,
                    loop: true,
                    items: thumbs,
                    margin: 10,
                    nav: true,
                    dots: false,
                    navText: ["&lsaquo;", "&rsaquo;"]
                })
                .on('click', '.owl-item', function() {
                    var i = $(this).index() - (thumbs);
                    sync2.trigger('to.owl.carousel', [i, duration, true]);
                    sync1.trigger('to.owl.carousel', [i, duration, true]);
                })
                .on('changed.owl.carousel', function(e) {
                    console.log(e.item.index - thumbs);
                    if (!flag) {
                        flag = true;
                        sync1.trigger('to.owl.carousel', [e.item.index - thumbs, duration, true]);
                        flag = false;
                    }
                });
        });
});