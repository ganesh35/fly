define([
    'jquery',
    'handlebars',
    'text!templates/roomPod/row.hbs'
], function($, Hbs, rowHbs) {
	
    var Row = function (data) {
    	var rowTemplate = Hbs.compile(rowHbs),
    		rowDOM = $(rowTemplate(data)),
    		guestsByRoom={},    		
    		 _ = {
			    	delete : function(e) {
			    		var $target = $(this),
				    		row = $target.closest('.pod-row'),
				    		pod = $target.closest('.room-pod'),
				    		rows = pod.find('.pod-row'),
				    		roomNo = row.data('roomno');
				    	roomNo !== 1 && roomNo === rows.length && row.remove(); // always can delete only the last row
				    	$('.del:disabled').last().prop('disabled', false); // enabling the last disabled item
			    	},
			    	change : function (e) {
			    		var $target = $(this),
			    			row = $target.closest('.pod-row'),
			    			type = $target.data('type'),
			    			content = $target.siblings('span'),
			    			val = +content.data('val') || 0;

			    		this.computeChildAge = function (val) {
			    			var $ageWrap = $('.age-wrap', row)
			    				i = 0;
		    				$ageWrap.empty();
		    				if (val) {
		    					while(i++ < val) 
		    						$('<label>Child ' + i + ' age <input type="number" min="0" placeholder="0 yrs" data-type="age" /></lable>').appendTo($('.age-wrap', row)); // re-manipulating DOM to prevent using cached value
		    				}
			    		};
			    		if ($target.hasClass('minus')) { // MINUS
			    			if (val === 0) // to prevent negative feeds 
			    				return false;
			    			val--;
			    		}
			    		else { // PLUS
			    			val++;
			    		}
			    		switch (type) {
			    			case 'adult': {
			    				var str = val > 1 ? ' Adults' : ' Adult';
			    				content.data('val', val).text(val + str);
			    				break;
			    			}
			    			case 'child': {
			    				var str = val > 1 ? ' Children' : ' Child';
			    				content.text(val + str)
			    					   .data('val', val);
			    				this.computeChildAge(val);

			    				break;
			    			}
			    		}
			    	},
			    	alterAge : function (e) {
			    		var $target = $(this),
			    			row = $target.closest('.pod-row'),
			    			val = +$target.val();
			    		console.log('Changed age - ', val);
			    	}
		    	}

		    	rowDOM.on('click', '.del', _.delete);
		    	rowDOM.on('click', 'a', _.change);
		    	rowDOM.on('change', 'input[type=number]', _.alterAge);

		    	return {
		    		dom : rowDOM
		    		
		    	}

		    };


    return Row;
});