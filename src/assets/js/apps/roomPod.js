define([
    'jquery',
    'handlebars',
    'apps/roomPod/row'
], function($, Hbs, Row) {
    var pod = $('.room-pod');
    var addRoom = $('.add-room', pod);

    var handlers = new function() {

        function add(e) {
            var $target = $(this),
                pod = $target.closest('.room-pod'),
                rows = pod.find('.pod-row'),
                data = {
                    roomNo: ++rows.length
                };

            var row = new Row(data);
            rows.find('.del').prop('disabled', true);
            row.dom.insertBefore(addRoom);
        }
        (add)(); // an explicit IIFE -- to have one default row on initialisation

        return {
            add: add,
            close: function(e) {
                var $target = $(this),
                    pod = $target.closest('.room-pod');
                pod.hide();
                e.stopPropagation();
            },
            done: function(e) {
                var $target = $(this);
                var $roomsList = $target.closest('.room-pod')
                var roomsCount = $roomsList.find('.pod-row');
                var adults = 0;
                var childrens = 0;
                $.each(roomsCount, function(i, val) {
                    var $current = $(this);
                    adults = adults + $current.find(".adult").data("val");
                    childrens = childrens + $current.find(".child").data("val");
                });
                // console.log(adults)
                $(".guestList").text(adults + " Adult, " + childrens + " Child");
                $(".roomsCount").text(roomsCount.length + " Room");
                $roomsList.hide();
                e.stopPropagation();
            }

        }

    };

    $('.add', pod).on('click', handlers.add);
    $('.done', pod).on('click', handlers.done);
    $('.close', pod).on('click', handlers.close);
});