// A simple runtime configuration file.  
require.config({
    paths: {
        jquery: './vendors/jquery-3.1.1.min',
        handlebars: './vendors/handlebars-v4.0.5',
        bootstrap: './vendors/bootstrap-3.3.7.min', 
        noUiSlider : './vendors/nouislider',
        globals:'globals',
        templates: './templates',
        apps: './apps',
        text: './vendors/text-2.0.15',
        owl:'./vendors/owl.carousel',
        moment:'./vendors/moment',
        daterangepicker:'./vendors/daterangepicker'
    },
    shim: {
        jquery: {
            exports: '$'
        },
        bootstrap: {
            deps: [
                'jquery'
            ],
            exports: 'Bootstrap'
        }, 
        owl: {
            deps: ['jquery'],
            exports:'owl'
        },
        noUiSlider:{
            deps: ['jquery'],
            exports:'noUiSlider'
        },
        daterangepicker:{
             deps:['jquery','bootstrap','moment']
        }

    }
});

